using System.Collections.Generic;
using System;
using System.Linq;


namespace Tries
{

    public class Trie
    {
        
        public static readonly char[] LetterSet =
        {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'v',
            'u', 'w', 'x', 'y', 'z', ' ', '_', '\'','#'
        };

        public static readonly char[] NumSet = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

        public Node root { get; private set; }
        private char[] charSet;


        public Trie(char[] chars)
        {
            charSet = chars;
            root = new Node(charSet);
        }
        
        public void Insert(string key)
        {
            int len = key.Length;
            Node currentNode = root;
            for (int i = 0; i < key.Length; i++)
            {
                // Check if the s[i] is already present in Trie
                char ch = key[i];
                Node nextNode = null;
                currentNode.child.TryGetValue(ch, out nextNode);
                if (nextNode == null) {
                    // If not found then create and mark it as child
                    nextNode = new Node(charSet);
                    currentNode.child[ch] = nextNode;
                }

                // Move to next Trie Node
                currentNode = nextNode;
                // If its the last character of the string, mark as terminal
                if (i == len - 1)
                    currentNode.isTerminal = true;
            }   
        }

        public Node Delete(string key, Node node,  int depth = 0)
        {
            
            // If tree is empty
            if (node == null)
                return null;
 
            // If last character of key is being processed
            if (depth == key.Length) {
 
                // This node is no more end of word after removal of given key
                if (node.isTerminal)
                    node.isTerminal = false;
 
                // If given is not prefix of any other word
                if (node.ChildCount == 0) {
                    node = null;
                }
                return node;
            }
 
            // If not last character, recur for the child obtained 
            char ch = key[depth];
            node.child[ch] =  Delete(key, node.child[ch], depth + 1);
 
            // If root does not have any child (its only child got
            // deleted), and it is not end of another word.
            if (node.ChildCount == 0 && node.isTerminal == false){
                node = null;
            }
            return node;
        }
        
        public List<string> GetMatchKeys(string key)    //todo: this could be optmized, since it restarts the search when query modified. However, possible optimization: could continue from last used node if user just added a letter to last query.
        {
            Node prevNode = root;
            string prefix = "";
            int len = key.Length;
            List<string> matches = new List<string>();

            for (int i = 0; i < len; i++)
            {
                prefix += key[i];
                char lastChar = prefix[i];
                // Find the Node corresponding to the last character of key which is pointed by prevNode of the Trie
                Node curNode = prevNode.child[lastChar];
                // If nothing found, break the loop as no more prefixes are going to be found.
                if (curNode == null) {
                    i++;
                    break;
                }
                
                if (i == len - 1) {
                    // last letter
                    AddNodeToResults(curNode, prefix, matches);
                }

                // Change prevNode for next loop
                prevNode = curNode;
            }
            
            return matches;
        }
        
        public void AddNodeToResults(Node curNode, string prefix, List<string> prefixMatches)
        {
            // Check if the string 'prefix' ends at this Node   If yes then add to matches list.
            if (curNode.isTerminal) {
                prefixMatches.Add(prefix);
            }

            // Find all the direct child nodes to the current Node and then call the function recursively
            for (int i = 0; i < charSet.Length; i++)
            {
                char letter = charSet[i];
                Node nextNode = curNode.child[letter];
                if (nextNode != null) {
                    AddNodeToResults(nextNode, prefix + letter, prefixMatches);
                }
            }
        }

        public string Sanitize(string query)
        { //only allow in query characters that are mapped in Trie.
            char[] chars = query.ToCharArray();
            chars = Array.FindAll<char>(chars, (c => charSet.Contains(c)));
            return new string(chars);
        }
    }
    
}
