using System;
using System.Collections.Generic;

namespace Tries
{
    public class Node
    {
        
        public Dictionary<char, Node> child;
        public bool isTerminal;

        public Node(char[] charSet)
        {
            child = new Dictionary<char, Node>();
            for (int i = 0; i < charSet.Length; i++)
            {
                char letter = charSet[i];
                child[letter] = null;
            }

            isTerminal = false;
        }

        public int ChildCount => new Func<int>(() =>
        {
            int count = 0;
            foreach (var val in child.Values)
            {
                if (val != null)
                    count++;
            }

            return count;
        })();
    }
    
}