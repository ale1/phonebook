using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Collections;

public class QueryManager : MonoBehaviour
{
    public Transform ContentContainer;
    public GameObject TableEntryPrefab;
    
    private string lastVal;

    private InputField input;
    private Text content;

    private List<UI_ContactEntry> _entries = new List<UI_ContactEntry>();
    
    public Phonebook PhoneBook;
    
    // Start is called before the first frame update
    void Start()
    {
        input = GetComponent<InputField>();
    }

    // Update is called once per frame
    void Update()
    {
        if(string.IsNullOrWhiteSpace(input.text) && _entries.Count > 1)  //not optimal to have this in update loop. would need to be replaced with behaviour inside custom input field rather than using unity component.
        {
            lastVal = String.Empty;
            List<UI_ContactEntry> entries = ContentContainer.GetComponentsInChildren<UI_ContactEntry>().ToList();
            Clear(entries);
        }
    }

    public void SendQuery(string val)
    {
        PhoneBook = GameManager.Instance.PhoneBook; 
        lastVal = val;

        if (val.Length <= 0)
            return;
        
        string queryKey = Phonebook.NameToKey(val);
        List<Phonebook.ContactData>  results = PhoneBook.SearchPrefix(queryKey, 50);
        
        List<UI_ContactEntry> discard = new List<UI_ContactEntry>(_entries); //list of entries to discard. starts assumong everyone will be discarded, and we remove from list if we want to keep them.
        
        foreach(var data in results)
        {
            string id = data.id;
            UI_ContactEntry entry = discard.Find(x => x.id == id);  //data in result already exists in table. lets keep it. 
            if(entry != null)
                discard.Remove(entry);
            else  
            { //data in result doesnt exist in table, lets create it.
                GameObject GO = Instantiate(TableEntryPrefab, ContentContainer);
                entry = GO.GetComponent<UI_ContactEntry>();
                entry.Setup(data, PhoneBook);
                entry.OnDelete += RedoLastQuery;
                entry.OnEdit += RedoLastQuery;
                _entries.Add(entry);
            }
        }
        Clear(discard);
        Sort();
    }

    public void RedoLastQuery(UI_ContactEntry entry)
    {
        //destroy edited or deleted entry, then redo last query to rebuild table with fresh data.
        List<UI_ContactEntry> markedForClear = new List<UI_ContactEntry>() {entry};  
        Clear(markedForClear);
        SendQuery(lastVal);
    }

    public void Sort() //called after deleting.
    {
        StartCoroutine(SortCoroutine());
    }

    private void Clear(List<UI_ContactEntry> range)
    {
        StopCoroutine(SortCoroutine()); //stop previous sort if active.
        float delay = 0.02f;
        for(int i = range.Count - 1; i >= 0; i--)
        {
            UI_ContactEntry entryToDelete = range[i];
            _entries.Remove(entryToDelete);
            entryToDelete.gameObject.SetActive(false);
            Destroy(entryToDelete.gameObject, delay * i);
        }
    }

    private IEnumerator SortCoroutine()
    {
        _entries.Sort((e1,e2)=>e1.id.CompareTo(e2.id));
        for(int i=0; i < _entries.Count; i++)
        {
            _entries[i].gameObject.SetActive(false);  //deactive while moving, because VerticalLayout Component in unity is shite.
            _entries[i].transform.SetSiblingIndex(i);
            yield return null;
        }

        for (int i = 0; i < _entries.Count; i++)
        {
            _entries[i].gameObject.SetActive(true);
        }

    }
    
    
    
}
