using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Phonebook;

public class UI_ContactEntry : MonoBehaviour
{
    public string id;
    [SerializeField] public Text nameField;
    [SerializeField] public Text phoneField;
    [SerializeField] public Text emailField;
    [SerializeField] public Text twitterField;

    private ContactData _data;
    private Phonebook _phonebook;

    public Action<UI_ContactEntry> OnEdit;
    public Action<UI_ContactEntry> OnDelete;
    
    public void Setup(ContactData data, Phonebook phoneBook)
    {
        id = data.id;
        nameField.text = data.name;
        phoneField.text = data.phone;
        emailField.text = data.email;
        twitterField.text = data.twitter;

        _data = data;
        _phonebook= phoneBook;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DeleteEntry()
    {
        _phonebook.DeleteContact(_data.id);
        OnDelete?.Invoke(this);
    }

    public void EditEntry()
    {
        _phonebook.EditContact(_data.id, Reverse(_data.phone), Reverse(_data.email), Reverse(_data.twitter));
        OnEdit?.Invoke(this);
     
    }
    
    //being used for testing edit button.  todo: remove and replace by proper edit panel.
    private static string Reverse( string s )
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse( charArray );
        return new string( charArray );
    }
    
}
