using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using static Phonebook;
using Tries;

public static class Saver
{
    private static string savePathBase = Application.persistentDataPath + "/";
    public static Dictionary<char, string> saveFiles = new Dictionary<char, string>();
    
    static Saver()
    {
        char[] letterSet = Trie.LetterSet;
        foreach (var ch in letterSet)
        {
            saveFiles[ch] = savePathBase + "phonedataFor("+ch+")";
        }
    }
    public static void Save(char letter, List<ContactData> chunk)
     {
         //saving in each letter bucket file.  
         string path = saveFiles[letter];
         FileStream fs = new FileStream(path, FileMode.Create);
         BinaryFormatter bf = new BinaryFormatter();
         bf.Serialize(fs, chunk);
         fs.Close();
     }

    public static void Load(Action<List<ContactData>> OnDataReceivedCallback)
    {
        foreach (var path in saveFiles.Values)
        {
            if (File.Exists(path))
            {
                List<ContactData> loadedData;
                using (Stream stream = File.Open(path, FileMode.Open))
                {
                    var bformatter = new BinaryFormatter();
                    loadedData = (List<ContactData>) bformatter.Deserialize(stream);
                }

                if (loadedData.Count > 0)
                    OnDataReceivedCallback(loadedData);
            }
            else
            {
                Debug.Log("no save file to load at:" + path);
            }
        }
    }

    public static void EraseFiles()
    {
        foreach (var path in saveFiles.Values)
        {
            if(File.Exists(path))
                File.Delete(path);
        }

    }
}

