using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Assertions;

public class GameManager : MonoBehaviour   
{

    #region  Singleton
    
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }
    #endregion


    public bool LoadSavedDataOnStart;
    public Phonebook PhoneBook = new Phonebook();
    
    //lets use this as a TDD driver for now...
    void Start()
    {
        if(LoadSavedDataOnStart)
            PhoneBook.Load();
    }

    [ContextMenu("ErasePhonebook")]
    void ErasePhonebook()
    {
        PhoneBook = new Phonebook();
        Saver.EraseFiles();
    }

    [ContextMenu("GenerateFakeData")]
    void GenerateFakeData()
    {
       PhonebookTests.GenerateFakeData(PhoneBook,10000);  
    }
    
    
    void Update()
    {
        
    }

    private void OnApplicationQuit()
    {
        PhoneBook.Save();
    }
}
