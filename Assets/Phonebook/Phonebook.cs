using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tries;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Phonebook
{
    [Serializable]
    public struct ContactData
    {
        public string id;
        public string name;
        public string phone;
        public string email;
        public string twitter;
    }

    public static string NameToKey(string input)
    {
        return input.ToLower();
    }

    public static string PhoneToKey(string input)
    {
        //phone keys only accept numbers
        StringBuilder sb = new StringBuilder();
        foreach (char c in input)
        {
            if ((c >= '0' && c <= '9')) {
                    sb.Append(c);
                }
        }
        return sb.ToString();
    }

    public Dictionary<string, ContactData> Directory;
    private Map<string, string> keyMap;

    //Trie
    private Trie TrieForLetterSearch = new Trie(Trie.LetterSet);
    private Trie TrieForNumSearch = new Trie(Trie.NumSet); 

    //constructor;
    public Phonebook()
    {
        Directory = new Dictionary<string, ContactData>();
        keyMap = new Map<string, string>(); //translates phoneKeys into nameKeys and back;
    }
    
    

    public bool AddSavedContact(ContactData data)
    {
        string nameKey = data.id;
        string phoneKey = PhoneToKey(data.phone);
        
        if (!Directory.ContainsKey(nameKey))
        {
            TrieForLetterSearch.Insert(nameKey);
            TrieForNumSearch.Insert(phoneKey);
            keyMap.Add(nameKey, phoneKey);
        }
        else
        {
            return false; // fail. contact with that name already exists.
        }
        
        Directory[nameKey] = data;

        return true; // success;
    }

    
    public bool AddContact(string name, string phone = "", string email = "", string twitter = "")
    {
        string nameKey = NameToKey(name); // sanitized name is now used as key and also uniqueID.
        string phoneKey = PhoneToKey(phone);
        
        if (keyMap.ContainsKey2(phoneKey))
            return false; //fail, cant add becase phone number is being used elsewhere
        
        //update trie if this new entry
        if (!Directory.ContainsKey(nameKey))
        {
            TrieForLetterSearch.Insert(nameKey);
            TrieForNumSearch.Insert(phoneKey);
            keyMap.Add(nameKey, phoneKey);
        }
        else
        {
            return false; // fail. contact with that name already exists. 
        }
        


        //update phonebook
        ContactData newData = new ContactData();
        newData.name = name;
        newData.phone = phone;
        newData.email = email;
        newData.twitter = twitter;
        newData.id = nameKey;

        Directory[nameKey] = newData;

        return true; // success;
    }

    public void DeleteContact(string name)
    {
        string nameKey = NameToKey(name);
        string phoneKey = keyMap.GetValueByKey1(nameKey);
        Directory.Remove(nameKey);

        TrieForLetterSearch.Delete(nameKey, TrieForLetterSearch.root);
        TrieForNumSearch.Delete(phoneKey, TrieForNumSearch.root);
    }
    
    public bool EditContact(string name, string newPhone = null, string newEmail = null, string newTwitter = null)
    {
        string nameKey = NameToKey(name);
        
        if (!Directory.ContainsKey(nameKey))
            return false;   //failed, cannot edit contact because it doesnt exist.
        
        ContactData data = Directory[nameKey];

        if (newPhone != null && newPhone != data.phone) //phone data is being edited. make sure to update tries and keymaps
        {
            string phoneKey = PhoneToKey(data.phone);
            string newPhoneKey =  PhoneToKey(newPhone);

            if (keyMap.ContainsKey2(newPhoneKey))
                return false; //fail, cant change phone number to one already used by another contact.
                
            TrieForNumSearch.Delete(phoneKey, TrieForNumSearch.root); //delete old node
            TrieForNumSearch.Insert(newPhoneKey); //create new node for new phonekey
            keyMap.AddOrUpdateByKey1(nameKey,newPhoneKey);
            data.phone = newPhone;
        }

        if (newEmail != null)
            data.email = newEmail;
        if (newTwitter != null)
            data.twitter = newTwitter;

        Directory[nameKey] = data;
        return true;
    }

    [Obsolete("this method is deprecated and should not be used for anything except legacy tests")]
    public List<string> QuickSearchPrefix(string query)  //only returns list of name keys, and not whole contactData.
    {
        string queryKey = NameToKey(query);
        List<string> nameKeys = TrieForLetterSearch.GetMatchKeys(queryKey);
        return nameKeys;
    }

    public List<string> QuickSearchNum(string query)  //reverse lookup, searches for phone num prefix but returns only namekeys.
    {
        string queryKey = NameToKey(query);
        List<string> phoneKeys = TrieForNumSearch.GetMatchKeys(queryKey);
        return phoneKeys.Select(x => keyMap.GetValueByKey2(x)).ToList();
    }
    
    public List<ContactData> SearchPrefix(string query,  int maxResults = 10000)
    {
        string queryKey = NameToKey(query);
        List<ContactData> results = new List<ContactData>();
        
        //detect if this is a number search
        bool isNumber = true;
        foreach (var letter in query)
        {
            if (!Char.IsDigit(letter)) {
                isNumber = false;
                break;
            }
        }

        int counter = -1;
        if (isNumber) {
            List<string> phoneKeys = TrieForNumSearch.GetMatchKeys(queryKey);
                foreach (var phonekey in phoneKeys)
                {
                    if (counter++ < maxResults) {
                        string nameKey = keyMap.GetValueByKey2(phonekey);
                        results.Add(Directory[nameKey]);
                    }
                }
        }
        else {
            List<string> nameKeys = TrieForLetterSearch.GetMatchKeys(queryKey);
            foreach (var nameKey in nameKeys)
            {
                if(counter++ < maxResults)
                    results.Add(Directory[nameKey]);
            }
        }
        
        return results;

    }
        
    #region Save-Load

    public void Save()
    {
        //todo: optimize and do this as threaded async operation.
        
        List<ContactData> contacts = Directory.Values.ToList();
        foreach (var key in Saver.saveFiles.Keys)
        {
            List<ContactData> chunk = contacts.FindAll(x=> x.id[0] == key);
            Saver.Save(key, chunk);
        }
    }

    public void Load()
    {
        Saver.Load(OnDataReceived);
    }

    private void OnDataReceived(List<ContactData> data)
    {
        // Rebuiling tries and phone directory. 
        //todo: maybe worth saving tries instead of rebuilding?
        foreach (var contact in data)
        {
            AddSavedContact(contact );
        }
    }
    
    #endregion

    }


