using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using static Phonebook;

public class PhonebookTests : MonoBehaviour
{

    [Header("WARNING, enabling tests will overwrite your save file")]
    public bool EnableTests;

    void Start()
    {
        if (!Application.isEditor || !EnableTests)
            return;

        Phonebook testbook = new Phonebook();
        testbook.AddContact("ale", "111001");
        testbook.AddContact("alex", "200000");
        testbook.AddContact("alexandra", "3000000");
        testbook.AddContact("legolas", "400000");
        testbook.AddContact("peter", "5000000");
        Assert.AreEqual(5, testbook.Directory.Count, "test: contacts can be added");
        
        
        #region phonebook-CRUD
        string query = "peter";
        List<string> results = testbook.QuickSearchPrefix(query);
        Assert.AreEqual(1, results.Count);
        Assert.AreEqual(results[0], "peter", "contact can be found with full name search");
        
        query = "lego";
        results = testbook.QuickSearchPrefix(query);
        Assert.AreEqual(1,results.Count, "contact can be found with prefix search");
        Assert.AreEqual("legolas",results[0], "contact can be found with prefix search");
        
        results = testbook.QuickSearchPrefix("potato");
        Assert.IsTrue(results.Count == 0,"phonebook can handle returning no matches");
        
        results = testbook.QuickSearchPrefix("ale");
        Assert.AreEqual(expected:3, results.Count, "prefix search returns all suitable matches" );
        Assert.IsTrue(results.Contains("ale") && results.Contains("alex") && results.Contains("alexandra"));

        results = testbook.QuickSearchPrefix("alex");
        Assert.AreEqual(expected:2, results.Count, "prefix doesnt show partial matches" );
        Assert.IsTrue(results.Contains("alex") && results.Contains("alexandra"));
        
        results = testbook.QuickSearchPrefix("alexa");
        Assert.AreEqual("alexandra",results[0]);
        Assert.IsTrue(results.Count == 1);
        
        testbook.AddContact("DarthVader","600000");
        results = testbook.QuickSearchPrefix("darthvader");
        Assert.IsTrue(results.Count == 1);

        results = testbook.QuickSearchPrefix("darthVADER");
        Assert.IsTrue(results.Count == 1);
        List<ContactData> data = testbook.SearchPrefix("darthVADER", 1);
        Assert.AreEqual("DarthVader", data[0].name, "searching with capitalization doesnt break search");
        
        Assert.IsFalse(testbook.AddContact("DarthVader", "70000"));
        results = testbook.QuickSearchPrefix("DarthVader");
        Assert.AreEqual(1,results.Count, "there can only be one Sith Lord in the galaxy. contacts cannot be added twice");
        
               
        testbook.AddContact("Chew bacca", "8723123");
        testbook.AddContact("Chewbacca", "8888222");
        results = testbook.QuickSearchPrefix("chew");
        Assert.IsTrue(results.Count == 2, "spacing between words counts as a distinct character");
        
        testbook.DeleteContact("peter");
        Assert.IsFalse(testbook.Directory.ContainsKey("peter"));
        results = testbook.QuickSearchPrefix("peter");
        Assert.IsTrue(results.Count == 0, "contacts can be safely deleted");
        
        testbook.AddContact("peterson", "474747");
        results = testbook.QuickSearchPrefix("peter");
        Assert.AreEqual("peterson", results[0], "deleted contacts dont interfere with search of contacts with shared nodes");
        
        testbook.AddContact("georgio", "222");
        Assert.IsFalse(testbook.AddContact("georgio","333"));
        Assert.IsTrue(testbook.Directory["georgio"].phone == "222");
        results = testbook.QuickSearchPrefix("georgio");
        Assert.AreEqual(1,results.Count); 
        Assert.IsTrue( results[0] == "georgio", "adding repeat contact doesnt create duplicate" );

        testbook.AddContact("baconForBreakfast", "12345");
        Assert.IsFalse(testbook.AddContact("baconForDinner", "12345"));
        results = testbook.QuickSearchNum("12345");
        Assert.AreEqual(1, results.Count, "trying to add distinct contacts that share numbers deoesnt break phonebook");
        Assert.AreEqual("baconforbreakfast", results[0], "adding distinct contacts with same number doesnt corrupt name");
        data = testbook.SearchPrefix("12345", 10);
        Assert.AreEqual("baconForBreakfast", data[0].name, "camelcase names return with appropriate capitalization");


        testbook.AddContact("cowabanga!;", "0009999");
        testbook.AddContact("cowabanga/", "00099992", "gello@jello.com");
        results = testbook.QuickSearchPrefix("cowabanga");
        Assert.AreEqual(0, results.Count, "adding forbidden special characters doesnt break phonebook");
        
        testbook.AddContact("O'Donnel", "0009999", "Odonnel@gmail.com","#ODonnie");
        results = testbook.QuickSearchPrefix("cowabanga");
        Assert.AreEqual(0, results.Count, "phonebook supports names like O'Donnel, with single quote.");
        
        int counter = 0;
        for(char ch = 'a'; ch < 'z'; ch++)  
        {
            testbook.AddContact(string.Concat("testMan_",ch), (555500000 + counter++).ToString(),string.Concat("email",ch,"@gmail.com"),"testmanTw"+ch);
        }
        results = testbook.QuickSearchPrefix("testMa");
        Assert.IsTrue( results.Count >  20, "contact name can have underscore character");
        
        testbook.EditContact("ale","47822319","hello@ale.com","#aleTwitter");
        data = testbook.SearchPrefix("ale", 10);
        Assert.AreEqual("ale",data[0].name, "editing contact returns newest data");
        Assert.AreEqual("47822319",data[0].phone);
        Assert.AreEqual("hello@ale.com",data[0].email);
        Assert.AreEqual("#aleTwitter",data[0].twitter);

        Assert.IsFalse(testbook.EditContact("McSalchicas", newPhone: "9834893421231234312"), "tring to edit non-existent contact should return false");
        
        

        
        
        #endregion
        
        #region reverseLookup
        results = testbook.QuickSearchNum("111");
        Assert.AreEqual(0,results.Count);
        results = testbook.QuickSearchNum("47822319");
        Assert.AreEqual(1,results.Count);
        Assert.AreEqual("ale",results[0], "can handle number queries of edited contact");
        
        testbook.AddContact("Crazy88", "881"); 
        testbook.AddContact("Big88", "882"); 
        testbook.AddContact("Lazy88", "883");
        data = testbook.SearchPrefix("88", 10);
        Assert.IsTrue(data.Exists(x=> x.name =="Crazy88"));
        Assert.IsTrue(data.Exists(x => x.name =="Big88"));
        Assert.IsTrue(data.Exists(x => x.name== "Lazy88"));
        #endregion
        
        
        #region save/loading
        testbook.Save();
        testbook = null;
        testbook = new Phonebook();
        Assert.IsTrue(testbook.Directory.Count == 0);
        testbook.Load();
        Assert.IsTrue(testbook.Directory.Count > 10, "phonebook can be saved and loaded");
        results = testbook.QuickSearchPrefix("alex");
        Assert.AreEqual(expected:2, results.Count, "post load prefix search returns all suitable matches" );
        Assert.IsTrue(results.Contains("alex") && results.Contains("alexandra"));
        #endregion

    }


    //util for generate fake data.  todo: move to own class.
    public static void GenerateFakeData(Phonebook phonebook, int amount)
    {
        int counter = 0;
        while(counter < amount)
        {
            var chars = "abcdefghijklmnopqrstuvwxyz";
            var nums = "01234567890";

            var nameChars = new char[8];
            var numChars = new char[8];
            var random = new System.Random();
            for (int i = 0; i < nameChars.Length; i++)
            {
                nameChars[i] = chars[random.Next(chars.Length)];
                numChars[i] = nums[random.Next(nums.Length)];
            }
            string name = new string(nameChars);
            string phone = new string(numChars);
            string email = name.Substring(4) + "@"+phone[0] + name[5]+".com";
            string twitter = "#" + name.Substring(0, 5) + phone[1];


            phonebook.AddContact(name, phone: phone, email:email, twitter: twitter);
            counter++;
        }
        
 


    }
}


