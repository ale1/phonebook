### AleTrie - 
This sample Trie app implements the basic funcionality of a phonebook.  It can add, edit, delete and search for contacts using a Trie approach.

# Basic usage


**Make sure you open it in Unity v 2020.3.5f1!**



```csharp
using Phonebook;
	
...

var phoneBook = new Phonebook();

phoneBook.AddContact(name: "Ale","4782-2319", "ale@gmail.com", "#aleTweets"  ); //returns true on success.
phoneBook.AddContact("Manu", phone: "23232");  //for partial contact info, use named params.  name is the only mandatory field.

phoneBook.AddContact("ale"); //returns false. this wont add a new contact, as names must be unique.
phoneBook.AddContact("evilAle", "4782-2319") //returns false.  this contact has non-unique phoine number, so cant be added.

phoneBook.DeleteContact("manu") //returns true.  we are no longer friends :-(

phoneBook.EditContact("ale", phone :"12345") // new phone number! editing ale's info.

phoneBook.EditContact("McPoop", phone: "12345", email: "spam@putin.ru") //returns false -> cant edit contact if doesnt exist, or uses non-unique phone.
```


## Searching

## prefix search


```csharp
/*
returns list of keys with prefix "al", i.e: ("ale", "alexa","alexandra")
note that only returns list of key strings, not the contactData for the matches.
*/
List<string> matchingKeys = phoneBook.QuickSearchPrefix("al"); 


//if you want your prefix searches to include contact data in results, use:
List<ContactData> matches = phoneBook.SearchPrefix("al",10); //where int is max number of results. 

//returned contact data can be read to extract any info on a particular match:
ContactData first = matches[0]
Debug.Log( "hello" + first.name, "with phone:" first.phone + "and email:" + first.email + "and twitter:" + first.twitter) 

//if you already have a key saved from a previous search, you can retrieve contact data directly from directory:
ContactData myData = Phonebook.Directory["ale"]; 
```

##suffix search

**Not implemented** -  however, available classes can be easily expanded to accomodate a suffix search:   
Implementation steps:

* (1) Create new Trie
* (2) when adding keys to Trie, reverse them.  I.e, key for "Peter" in regular trie is "peter"
but fur suffix search, it would be "retep";
* (3) when processing suffix searches, reverse the player input before sending it to suffixTrie.  
  I.e "ter" -> "ret", which when sent to suffixTrie will return key "retep" which can be reversed again to key original "peter", which is retrievable in directory. 



##infix search

**Not Implemented**. -  however, would work the same as prefix search, except that new tries should be created with alternating truncations. 
* (1)  e.g: Peter would have multiple Tries, covering all possbile truncation combinations from front and back of the word: so that "ete", "et", "te",  are keyed.  

 

## reverse lookup (phone to name)

```csharp
phoneBook.QuickSearchNum("12345");
```
will return name Key that has that phone number.  
you can use that key to retrieve its data directly from `Phonebook.Directory[key]`.



## Notes
 
* Note that editing contacts works through script, but doing it through the UI is currently NOT implemented.  
  The edit button that is visible will simply reverse some data and refresh the UI.
  
  
* Sorting is automatically done alphabetically.  
  For date sort, implementation should be trivial by adding dateAdded field to contact data and using same Sort algorithm that is already in place in `QueryManager`.
 
  

## implementation

There are number of optimizations that have not been implemented, but are noted in the code.

Below is a short description of the main clases.



Class|Description  
-----|-------------
`Phonebook` |  At its core, its a dictionary mapping name keys to Contacts.   In this implementation, unique contact names are used as keys / unique ids for hashed contact names.
`Trie` | Trie logic for searching nodes (only Prefix search for now). Currently available in two flavors: letter search (for searching names) and number search for searching phone numbers.  More can be created and linked to phonebook (i.e for searching email, twiter, etc). 
`Node` | Basic building block of any Trie implementation.  Modify at your own peril.
`QueryManager` | monobehaviour in the scene that talks between phonebook and ui showing queries in the frontend.
`ContactData` | small class that acts as a serializable data container.  primitives type fields can be added and they will be saved.
`Map` | An extension class used to generate bi-directional dictionaries.  Allows for mapping phone keys to name keys, and thus facilitating  numberTrie operations to quickly transalte its keys into the appropriate name key to find contactData in Phonebook. 
`Saver`| Crude saving class that separates contactData into first-letter 'buckets' (a-z) or (0-9) and saves into equal number of binary files.  App saves automatically on application exit.  Loading is automatic on startup if enabled in gameManager script.
`PhonebookTests`| A comprehensive coverage of unit tests for TDD. there are still some edge cases that could be added. But for basic CRUD, it does the job.  Always run in testMode after touching code. (Tests enabled in Scene component).   Be careful since test-mode erases your previous save files. 
`DataGenerator`| **[not implemented]** a class for encapsulating fake data management and generating better fake data. 

## Utils and Helpers

* *GameManager in scene > ContextMenu > EraseSaveFiles* --> Allows you to Erase Save Files. works in editor mode.

* *GameManager in scene > ContextMenu > GenerateFakeData* --> Will add ~10k random entries to your phonebook. Can be stacked multiple times in edit or runtime.



 



